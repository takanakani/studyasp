﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WebAPI_Common.Models
{
    [DataContract]
    public class ResponseInfoPutFile
    {
        [DataMember]
        public int PutFileCount { get; set; }

        public ResponseInfoPutFile(int putFileCount)
        {
            PutFileCount = putFileCount;
        }
    }
}
