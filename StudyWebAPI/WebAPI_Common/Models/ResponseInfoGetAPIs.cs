﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WebAPI_Common.Models
{
    [DataContract]
    public class ResponseInfoGetAPIs
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Assembly { get; set; }

        [DataMember]
        public string Version { get; set; }

        [DataMember]
        public Type ParameterType { get; set; }

        [DataMember]
        public Type ReturnType { get; set; }
    }
}
