﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPI_Common.Models;

namespace WebAPI_NETCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommandsController : Controller
    {
        public static List<ResponseInfoGetAPIs> APIInfos = new List<ResponseInfoGetAPIs>()
        {
            new ResponseInfoGetAPIs(){Id = "GetFile", Name =@"ファイルダウンロード", ParameterType = typeof(RequestInfoGetFile), ReturnType = typeof(File)},
            new ResponseInfoGetAPIs(){Id = "PutFile", Name =@"ファイルアップロード", ParameterType = null, ReturnType = typeof(ResponseInfoPutFile)},
            new ResponseInfoGetAPIs(){Id = "GetFileList", Name =@"ファイル一覧取得", ParameterType = typeof(RequestInfoGetFile), ReturnType = typeof(ResponseInfoGetFileList)},
            new ResponseInfoGetAPIs(){Id = "GetFileTimeStamp", Name =@"ファイル更新日時取得", ParameterType = typeof(RequestInfoGetFileLastWriteTime), ReturnType = typeof(ResponseInfoGetFileLastWriteTime)},
        };

        ////シリアライザーのインスタンスを生成
        //private DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(RequestInfo));

        /// <summary>
        /// APIのリストを取得します。
        /// </summary>
        /// <returns>結果</returns>
        /// <remarks>GET api/commands</remarks>
        [HttpGet]
        public IEnumerable<ResponseInfoGetAPIs> GetAll() => APIInfos;

        /// <summary>
        /// ファイルをダウンロードします。
        /// </summary>
        /// <param name="model">パラメータ</param>
        /// <returns>結果</returns>
        /// <remarks>POST api/commands/GetFile</remarks>
        [HttpPost("GetFile")]
        public IActionResult Post(RequestInfoGetFile model)
        {
            var bytes = System.IO.File.ReadAllBytes(Path.Combine("d:\\", model.FileName));
            return File(bytes, "application/octet-stream");
        }

        /// <summary>
        /// ファイルをアップロードします。
        /// </summary>
        /// <param name="files"></param>
        /// <returns>アップロードしたファイルの数</returns>
        /// <remarks>POST api/commands/PutFile</remarks>
        [HttpPost("PutFile")]
        public async Task<IActionResult> Post(List<IFormFile> files)
        {
            long size = files.Sum(f => f.Length);
            var filePath = Path.GetTempFileName();

            foreach (var formFile in files)
            {
                if (formFile.Length > 0)
                {
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await formFile.CopyToAsync(stream);
                    }
                }
            }

            return Ok(new ResponseInfoPutFile(files.Count));
        }

        /// <summary>
        /// ファイルリストを取得します。
        /// </summary>
        /// <param name="model">パラメータ</param>
        /// <returns>結果</returns>
        /// <remarks>POST api/commands/GetFileList</remarks>
        [HttpPost("GetFileList")]
        public IActionResult Post(RequestInfoGetFileList model)
        {
            var files = System.IO.Directory.GetFiles(Path.Combine("d:\\", model.DirectoryName)).ToList<string>();
            return Ok(new ResponseInfoGetFileList(files.ToList<string>()));
        }

        /// <summary>
        /// ファイルのタイムスタンプを取得します。
        /// </summary>
        /// <param name="model">パラメータ</param>
        /// <returns>結果</returns>
        /// <remarks>POST api/commands/GetFileTimeStamp</remarks>
        [HttpPost("GetFileTimeStamp")]
        public IActionResult Post(RequestInfoGetFileLastWriteTime model)
        {
            var fileLastWriteTime = System.IO.File.GetLastWriteTime(Path.Combine("d:\\", model.FileName));
            return Ok(new ResponseInfoGetFileLastWriteTime(fileLastWriteTime));
        }

    }
}
