﻿using System;
using System.Runtime.Serialization;

namespace WebAPI_Common.Models
{
    [DataContract]
    public class RequestInfoGetFileList
    {
        [DataMember]
        public string DirectoryName { get; set; }
    }
}
