﻿using System;
using System.Runtime.Serialization;

namespace WebAPI_Common.Models
{
    [DataContract]
    public class RequestInfoGetFile
    {
        [DataMember]
        public string FileName { get; set; }
    }
}
