﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WebAPI_Common.Models
{
    [DataContract]
    public class ResponseInfoGetFileList
    {
        [DataMember]
        public List<string> Files { get; set; }

        public ResponseInfoGetFileList(List<string> files )
        {
            Files = files;
        }
    }
}
