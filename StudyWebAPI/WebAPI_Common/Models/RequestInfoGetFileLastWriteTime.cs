﻿using System;
using System.Runtime.Serialization;

namespace WebAPI_Common.Models
{
    [DataContract]
    public class RequestInfoGetFileLastWriteTime
    {
        [DataMember]
        public string FileName { get; set; }
    }
}
