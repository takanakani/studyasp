﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace WebAPI_Common.Models
{
    [DataContract]
    public class ResponseInfoGetFileLastWriteTime
    {
        [DataMember]
        public DateTime FileLastWriteTime { get; set; }

        public ResponseInfoGetFileLastWriteTime(DateTime fileLastWriteTime)
        {
            FileLastWriteTime = fileLastWriteTime;
        }
    }
}
